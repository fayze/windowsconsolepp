#ifndef IMAGE_H
#define IMAGE_H

#include "SDL.h"
#include "SDL_image.h"
#include "defs.h"

class Image
{
    public:
        Image(SDL_Renderer *renderer,const char *fichier,bool *isRunning);
        ~Image(void);
        void setRect(int x,int y,int w,int h);
        void draw(void);
        void setX(int x);
        void setY(int y);
        void setXY(int x,int y);
        int getX(void) const{return m_posImage.x;};
        int getY(void) const{return m_posImage.y;};
        int getTextureW(void){return textureW;};
        int getTextureH(void){return textureH;};



    private:
        SDL_Texture *m_image;
        SDL_Rect m_posImage;
        SDL_Renderer *m_renderer;
        int textureW;
        int textureH;
        int timeur;
};

#endif // IMAGE_H
