#include "CMain.h"
#include <iostream>
using namespace std;

CMain::CMain()
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    window = NULL;
    window=SDL_CreateWindow("Template SDL2",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,SCREEN_W,SCREEN_H,SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    renderer=NULL;
    renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    if(window==NULL)
    {
        cout<<"Impossible de creer la fenetre";
    }

    SDL_Surface *icon=NULL;
    icon=IMG_Load("pics/logo.png");
    if(icon==NULL)
        cout<<"ERROR (loading logo): "<<IMG_GetError()<<endl;
    SDL_SetWindowIcon(window,icon);
    SDL_FreeSurface(icon);
    icon=NULL;
}

void CMain::Cbegin(void){
SDL_RenderClear(renderer);
}

void CMain::Cend(void){
SDL_RenderPresent(renderer);
}

CMain::~CMain()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    //dtor
}
