#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

#include "Music.h"

void dPerror(DIR* dir);
void denombrer(DIR *dir,int *nFile,int *nRep);
void printDir(DIR *dir);
void viderBuffer(void);
int lire(char chaine[],int len);
int isDir(char *chaine);
void windows_clear_screen(void);
int lireNomDossier(char *chaine,char *entree);
void reculerDossier(char *chaine);
void readRepertoire(DIR *dir,char *chaine,Music music);
int strDstr(char *chaine,const char* str);
int isInDir(DIR *dir,const char *str);
int present(const char *filepath);
int fRename(const char* temp);
int fRemove(const char *temp);
int copy_file(char const * const source, char const * const destination);
int fCopy(char const * const temp);
int fCopy2 (const char *chaine,const char *temp);
int retirer(char *chaine,char *temp,const char *str1,const char* str2);
int fMove(const char* chaine,const char* temp);
int fsearch(const char *chaine,const char* temp);
int createALaunchBatFile(const char *chaine);
int fView(const char* chaine,const char* temp);

#endif // FONCTIONS_H_INCLUDED
