#include "Image.h"
#include <iostream>


using namespace std;
Image::Image(SDL_Renderer *renderer,const char *fichier,bool *isRunning)
{
    m_image=NULL;
    m_renderer=NULL;
    m_renderer=renderer;

    m_posImage.x=0;
    m_posImage.y=0;
    m_posImage.w=0;
    m_posImage.h=0;
    textureW=0;
    textureH=0;

    m_image=IMG_LoadTexture(m_renderer,fichier);
    if(m_renderer==NULL)
    {
        cout <<"Impossible erreur de passage de renderer "<<SDL_GetError();
        *isRunning=false;
    }
    if(m_image==NULL)
    {
        cout <<"Impossible de charger l'image "<<SDL_GetError();
        *isRunning=false;
    }
    SDL_QueryTexture(m_image,NULL,NULL,&textureW,&textureH);
    m_posImage.w=textureW;
    m_posImage.h=textureH;

    //ctor
}

void Image::draw(void){
    SDL_RenderCopy(m_renderer,m_image,NULL,&m_posImage);
}

void Image::setRect(int x,int y,int w,int h){
    m_posImage.x=x;
    m_posImage.y=y;
    m_posImage.w=w;
    m_posImage.h=h;
}

void Image::setX(int x){
m_posImage.x=x;
}

void Image::setY(int y){
m_posImage.y=y;
}

void Image::setXY(int x,int y){
m_posImage.x=x;
m_posImage.y=y;
}


Image::~Image(void)
{
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyTexture(m_image);
    //dtor
}
