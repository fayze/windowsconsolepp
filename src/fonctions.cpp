#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dirent.h>
#include "fonctions.h"
#include <windows.h>

using namespace std;

typedef struct dirent dirent;

void dPerror(DIR* dir)
{
    if(dir==NULL)
    {
        perror("");
        ;
    }
}

void denombrer(DIR *dir,int *nFile,int *nRep)
{
    int a=0,b=0;
    dPerror(dir);
    dirent *rep=NULL;
    rewinddir(dir);
    while((rep=readdir(dir))!=NULL)
    {
        if(strcmp(rep->d_name,".")!=0 && strcmp(rep->d_name,"..")!=0)
        {
            if(strchr(rep->d_name,'.')==NULL)
                b++;
            else
                a++;
        }
    }

    *nFile=a;
    *nRep=b;
}

void printDir(DIR *dir)
{
    dirent *rep=NULL;
    printf("\n----DIRECTORY BEGINNING----\n");
    while((rep=readdir(dir))!=NULL)
    {
        if(strchr(rep->d_name,'.')==NULL)
            printf("\n-> <DIR> %s",rep->d_name);
        else
            printf("\n->       %s",rep->d_name);

    }
    printf("\n\n----DIRECTORY END----------\n");
}

void viderBuffer(void)
{
    int c=0;
    while(c!='\n' && c!=EOF)
        c=getchar();
}

int lire(char *chaine,int len)
{

    char *pos=NULL;
    if(fgets(chaine,len,stdin)!=NULL)
    {
        pos=strchr(chaine,'\n');
        if(pos!=NULL)
            *pos='\0';
        else
        {
            viderBuffer();
            printf("\nDesole chaine trop grande sortie...\n");
            exit(-1);
        }
    }
    else
    {
        viderBuffer();
        perror("");
        exit(-1);
    }

    return 1;

}

void reculerDossier(char *chaine)
{
    char *i=NULL;
    for(i=chaine+strlen(chaine)-1; i>=chaine && *(i-1)!='/' && *(i-1)!='\\'; i--);
    *i='\0';
}

int isInDir(DIR *dir,const char *str)
{
    dirent *rep=NULL;
    int a=-1;
    while((rep=readdir(dir))!=NULL)
    {
        if(strcmp(rep->d_name,str)==0)
        {
            printf("\a");
            a=1;
        }
    }

    return a;
}

int lireNomDossier(char *chaine,char *entree)
{
    char temp[250]="";

    lire(temp,200);

    if(strchr(temp,':')!=NULL && strchr(temp,' ')==NULL)
    {
        strcpy(chaine,temp);
    }
    else if(strcmp(temp,"..")==0)
    {
        reculerDossier(chaine);
    }
    else if(strstr(temp,"./")!=NULL ||strstr(temp,"../")!=NULL ||strstr(temp,".\\")!=NULL ||strstr(temp,"..\\")!=NULL)
    {
        strcat(chaine,temp);
    }
    else
    {

        strcat(chaine,temp);

        if(strchr(temp,'.')!=NULL)
        {
            strcpy(entree,temp);
            return -1;
        }
    }

    strcpy(entree,temp);
    return 1;
}

int isDir(char *chaine)
{
    if(chaine==NULL)
    {
        printf("\nERROR the string is NULL\n");
        return -1;
    }

    if(strchr(chaine,'/')==NULL && strchr(chaine,'\\')==NULL)
    {
        if(strchr(chaine,'.')==NULL)  //AUCUN / OU \\ ALORS C'EST UN DOSSIER DANS LE CAS CONTRAIRE C'EST UN FICHIER
            return 1;
        else
            return 0;
    }
    else
        return 1;
}

void windows_clear_screen(void)
{
    HANDLE hConsole;
    CONSOLE_SCREEN_BUFFER_INFO Info;
    DWORD NbOctetsEcrits; /* Requis par FillConsoleOutputCharacter */
    COORD Debut = {0, 0};

    /* STD_OUTPUT_HANDLE fait r�f�rence a la sortie standard du programme qui est par d�faut la console */
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    /* Lit les infos sur le buffer de l'�cran */
    GetConsoleScreenBufferInfo(hConsole, &Info);

    /* Remplit l'�cran avec le caract�re espace */
    FillConsoleOutputCharacter(hConsole, ' ', Info.dwSize.X*Info.dwSize.Y, Debut, &NbOctetsEcrits);

    /* Remet le curseur au d�but de l'�cran */
    SetConsoleCursorPosition(hConsole, Debut);
}


int strDstr(char *chaine,const char* str)
{
    if(str==NULL || chaine==NULL)
        return -1;

    char temp[100]="";
    char *ptr=NULL;
    unsigned i=0,j=0,n=strlen(chaine),a=0;
    ptr=strstr(chaine,str);
    if(ptr==NULL)
        return -1;

    for(i=0; i<n; i++)
    {
        a=ptr-chaine;
        if(i<a || i>=a+strlen(str))
            temp[j++]=chaine[i];
    }

    strcpy(chaine,temp);
    return 1;
}


void readRepertoire(DIR *dir,char *chaine,Music music)
{
    char temp[250]="";
    int a=-1;
debut:
    strcpy(temp,"");
    printf("\n<<%s>> ",chaine);
    a=-1;
    a=lireNomDossier(chaine,temp);

    if(strcmp(temp,"exit")==0)
        exit(0);
    else if(strstr(temp,"rename ")!=NULL || strstr(temp,"ren ")!=NULL)
    {
        //retirer(chaine,temp,"rename ","ren ");
        fRename(chaine);
        strDstr(chaine,temp);
        goto debut;
    }
    else if(strstr(temp,"remove ")!=NULL || strstr(temp,"rm ")!=NULL)
    {
        retirer(chaine,temp,"remove ","rm ");
        fRemove(chaine);
        strDstr(chaine,temp);
        goto debut;
    }
    else if(strstr(temp,"copy ")!=NULL || strstr(temp,"cp ")!=NULL)
    {
        retirer(chaine,temp,"copy ","cp ");
        strDstr(chaine,temp);
        fCopy2(chaine,temp);
        goto debut;
    }
    else if(strstr(temp,"move ")!=NULL || strstr(temp,"mv ")!=NULL)
    {
        retirer(chaine,temp,"move ","mv ");
        strDstr(chaine,temp);
        fMove(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!play ")!=NULL || strstr(temp,"!py ")!=NULL)
    {
        retirer(chaine,temp,"!play ","!py ");
        music.changeMusic(chaine);
        music.playMusic(0);
        strDstr(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!pause")!=NULL || strstr(temp,"!p")!=NULL)
    {
        retirer(chaine,temp,"!pause","!p");
        music.pauseMusic();
        strDstr(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!resume")!=NULL || strstr(temp,"!r")!=NULL)
    {
        retirer(chaine,temp,"!resume","!r");
        music.resumeMusic();
        strDstr(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!stop")!=NULL || strstr(temp,"!s")!=NULL)
    {
        retirer(chaine,temp,"!stop","!s");
        music.haltMusic();
        strDstr(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!find ")!=NULL || strstr(temp,"!f ")!=NULL)
    {
        retirer(chaine,temp,"!find ","!f ");
        strDstr(chaine,temp);
        fsearch(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!cbfile")!=NULL || strstr(temp,"!cbf")!=NULL)
    {
        retirer(chaine,temp,"!cbfile","!cbf");
        strDstr(chaine,temp);
        createALaunchBatFile(chaine);
        goto debut;

    }
    else if(strstr(temp,"start ")!=NULL || strstr(temp,"!run ")!=NULL)
    {
        system(temp);
        retirer(chaine,temp,"start ","!run ");
        strDstr(chaine,temp);
        goto debut;

    }
    else if(strstr(temp,"!home")!=NULL || strstr(temp,"!h")!=NULL)
    {
        strcpy(chaine,"./");
    }
    else if(strstr(temp,"!view ")!=NULL || strstr(temp,"!vw ")!=NULL)
    {
        retirer(chaine,temp,"!view ","!vw ");
        strDstr(chaine,temp);
        fView(chaine,temp);
        goto debut;

    }

    if(a>0)
    {

        if(chaine[strlen(chaine)-1]!='/')
            strcat(chaine,"/");
        closedir(dir);
        dir=opendir(chaine);
        if(dir==NULL)
        {
            printf("ERROR (impossible d'ouvrir %s)\n",temp);
            perror("");
            strDstr(chaine,temp);
            goto debut;
            exit(-1);
        }
        windows_clear_screen();
        printDir(dir);

    }
    else
    {
        present(chaine);
        reculerDossier(chaine);
    }



    readRepertoire(dir,chaine,music);

}

int present(const char *filepath)
{
    FILE *fichier=NULL;
    char chaine[200][1000]= {{""}};
    int n=0,i=0;
    if(filepath==NULL)
        return -1;

    fichier=fopen(filepath,"r");
    if(fichier==NULL)
    {
        perror("");
        return -1;
    }

    while(fgets(chaine[n],990,fichier)!=NULL)
    {
        n++;
    }

    printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

    for(i=0; i<n; i++)
        printf("%s\n",chaine[i]);
    printf("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    if(fclose(fichier)<0)
        perror("");
    return 1;
}

int fRename(const char* temp)
{
    if(strstr(temp,"rename ")==NULL && strstr(temp,"ren ")==NULL )
        return -1;

    char chaine[250]="";
    char oldName[200]="";
    int i=0;
    strcpy(chaine,temp);

    if(strstr(temp,"rename ")!=NULL)
        strDstr(chaine,"rename ");
    else if(strstr(temp,"ren ")!=NULL)
        strDstr(chaine,"ren ");
    else
        return -1;

    while(chaine[i]!=' ')
    {
        oldName[i]=chaine[i];
        i++;
    }
    strDstr(chaine,oldName);
    strDstr(chaine," ");

    if(chaine==NULL)
        return -1;

    if(strchr(chaine,'/')==NULL || strchr(chaine,'\\')==NULL)
    {
        char path[200]="";
        strcpy(path,oldName);
        reculerDossier(path);

       // if(strchr(path,'/')==NULL || strchr(path,'\\')==NULL)
         //   strcpy(path,"");

        strcat(path,chaine);
        strcpy(chaine,path);
    }
    if(rename(oldName,chaine)<0)
    {
        perror("");
        return-1;
    }
    else
        cout<<"success! file: \""<<oldName<<"\"changed into \""<<chaine<<"\" "<<endl;

    return 1;
}

int fRemove(const char *temp)
{
    char fileName[200]="";
    strcpy(fileName,temp);

    if(remove(fileName)<0)
    {
        perror("");
        return -1;
    }
    else
    {
        printf("SUCCESS: %s removed!\n",temp);
        return 1;
    }
}


int copy_file(char const * const source, char const * const destination)
{
    double taille=0,resultat=0,pre=0;
    FILE* fSrc;
    FILE* fDest;
    char buffer[512];
    int NbLus;

    if ((fSrc = fopen(source, "rb")) == NULL)
    {
        perror("source ERROR: ");
        printf("%s\n",source);
        return -1;
    }

    if ((fDest = fopen(destination, "wb")) == NULL)
    {
        perror("destination ERROR: ");
        printf("%s\n",destination);
        fclose(fSrc);
        return -2;
    }


    fseek(fSrc, 0, SEEK_END);
    taille=ftell(fSrc);
    rewind(fSrc);

    while ((NbLus = fread(buffer, 1, 512, fSrc)) != 0)
    {
        fwrite(buffer, 1, NbLus, fDest);
        resultat=(ftell(fSrc)/taille)*100;
        if((int)(resultat)>(int)pre)
        {

            windows_clear_screen();
            printf("\n\n\n>>>>>>>>>>>>\t%.0f%%\n\n",resultat);
            pre=resultat;
        }

    }

    fclose(fDest);
    fclose(fSrc);

    return 1;
}

int fCopy2 (const char *chaine,const char *temp)
{
    char originalFile[200]="",src[200]="";
    char cloneFile[200]="",dest[200]="";
    strcpy(src,chaine);
    strcpy(cloneFile,temp);
    if(src==NULL || cloneFile==NULL)
    {
        perror("");
        return -1;
    }

    int i=0;

    while(temp[i]!=' ')
    {
        originalFile[i]=temp[i];
        i++;
    }
    originalFile[i]='\0';
    strDstr(cloneFile,originalFile);
    strDstr(cloneFile," ");

    if(strchr(cloneFile,':')==NULL)
    {
        strcpy(dest,chaine);
    }

    strcat(src,originalFile);
    strcat(dest,cloneFile);

    if(copy_file(src,dest)<0)
    {
        /*chaine= <<%s>>\ntemp= <<%s>>\noriginalFile= <<%s>>\ncloneFile= <<%s>>\n*/
        printf("src= <<%s>>\ndest= <<%s>>\n",src,dest);
        return -1;
    }
    else
    {
        printf("SUCCESS: %s copied to %s\n",src,dest);
    }

    return 1;
}

int fCopy(char const * const temp)
{

    char src[250]="";
    char dest[200]="";
    int i=0;
    strcpy(src,temp);


    while(src[i]!=' ')
    {
        dest[i]=src[i];
        i++;
    }
    strDstr(src,dest);
    strDstr(src," ");

    if(src==NULL)
        return -1;

    if(strchr(src,'/')==NULL || strchr(src,'\\')==NULL)
    {
        char path[200]="";
        strcpy(path,dest);
        reculerDossier(path);

        if(strchr(path,'/')==NULL || strchr(path,'\\')==NULL)
            strcpy(path,"");

        strcat(path,src);
        strcpy(src,path);
    }

    if(copy_file(dest,src)<0)
        return-1;

    return 1;
}

int retirer(char *chaine,char *temp,const char *str1,const char* str2)
{
    if(strstr(temp,str1)!=NULL)
    {
        strDstr(temp,str1);
        strDstr(chaine,str1);
    }
    else if(strstr(temp,str2)!=NULL)
    {
        strDstr(temp,str2);
        strDstr(chaine,str2);
    }
    else
        return -1;

    return 1;
}

int fMove(const char* chaine,const char* temp)
{
       char originalFile[200]="",src[200]="";
    char cloneFile[200]="",dest[200]="";
    strcpy(src,chaine);
    strcpy(cloneFile,temp);
    if(src==NULL || cloneFile==NULL)
    {
        perror("");
        return -1;
    }

    int i=0;

    while(temp[i]!=' ')
    {
        originalFile[i]=temp[i];
        i++;
    }
    originalFile[i]='\0';
    strDstr(cloneFile,originalFile);
    strDstr(cloneFile," ");

    if(strchr(cloneFile,':')==NULL)
    {
        strcpy(dest,chaine);
    }

    strcat(src,originalFile); //copie du chemin complet du fichier a deplacer

    if(cloneFile[strlen(cloneFile)-1]!='/')  //si absence de / (car cloneFile contient le nom d'un dossier) en rajouter
        strcat(cloneFile,"/");
    strcat(cloneFile,originalFile); //rajouetr a clonefile le nom du fichier a deplacer
    strcat(dest,cloneFile); //chemin complet du npuvel emplacement du fichier + nom du ficher

    if(copy_file(src,dest)<0) //copie du ficher vers dest
    {
        /*chaine= <<%s>>\ntemp= <<%s>>\noriginalFile= <<%s>>\ncloneFile= <<%s>>\n*/
        printf("src= <<%s>>\ndest= <<%s>>\n",src,dest);
        return -1;
    }
    else
    {
        if(fRemove(src)>0) //suppresion de l' original du fichier deplac�
            printf("le fichier %s a �t� d�plac� vers %s avec SUCCESS!!\n",src,dest);
    }

    return 1;
}


int fsearch(const char *chaine,const char* temp)
{
    DIR *dir=NULL;
    dir=opendir(chaine);
    dPerror(dir);
    dirent *rep=NULL;
    bool isThere=false;

    while((rep=readdir(dir))!=NULL)
    {
        if(strstr(rep->d_name,temp)!=NULL)
        {
            printf("\n->       %s",rep->d_name);
            isThere=true;
        }
    }

    if(!isThere)
        cout<<"INFO: \""<<temp<<"\" NOT FOUND in dir "<<chaine<<endl;

    return 1;
}


int createALaunchBatFile(const char *chaine)
{
    DIR *dir=NULL;
    dir=opendir(chaine);
    dPerror(dir);
    dirent *rep=NULL;
    FILE *fichier=NULL;
    string path=chaine;
    path+="/launch.bat";
    cout<<path<<endl;

    fichier=fopen(path.c_str(),"w+");

    if(fichier==NULL)
    {
        perror("ERREUR ");
        return -1;
    }
    char str[200]="";
    fprintf(fichier,"@echo off\n");
    while((rep=readdir(dir))!=NULL)
    {
        if(strstr(rep->d_name,".exe")!=NULL)
        {
            strcpy(str,rep->d_name);
            strDstr(str,".exe");
            fprintf(fichier,"start %%~dp0%s /stext %%~dp0\\files\\%s.txt\n",rep->d_name,str);
        }
    }

    fclose(fichier);

    return 1;
}

int fView(const char* chaine,const char* temp)
{
    char file[200]="";
    FILE *data=fopen("data.tfsz","w+");
    if(data==NULL)
    {
        perror("");
        fclose(data);
        return -1;
    }
    else
    {
        if(strchr(temp,':')==NULL)
        {
            strcpy(file,chaine);
            strcat(file,temp);
        }
        else
            strcpy(file,temp);

        fprintf(data,"%s",file);
        fclose(data);
    }
//    system("start \"module image.exe\"");
    ShellExecute(NULL,  // Handle de la fen�tre parent
                 "open", // Action � effectuer
                 "module image.exe", // Fichier
                 "", // Param�tres
                 "", // R�pertoire par d�faut
                 SW_SHOWDEFAULT // Mani�re d'afficher
                 );
    //system("timeout 5");
    return 1;
}
