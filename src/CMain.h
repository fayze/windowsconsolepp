#ifndef CMAIN_H
#define CMAIN_H
#include "defs.h"
#include "SDL.h"
#include "SDL_image.h"

class CMain
{
    public:
        CMain();
        ~CMain();
        SDL_Renderer* getRenderer(void){return renderer;};
        void Cbegin(void);
        void Cend(void);


    private:
        SDL_Window  *window;
        SDL_Renderer *renderer;
};

#endif // CMAIN_H
