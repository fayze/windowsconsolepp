# Console ➕➕

Ceci est une sorte de petite ligne de commande personalisée comme jouer de la musique directement :D 
Par contre le code est VILAIN tres vilain.

## Liste des commandes possibles avec ce programme

* `../`	reculer d'un dossier
* `rename`	ren	renomer un fichier
* `remove`	rm	supprimer un fichier
* `copy`	cp	copier un fichier
* `move`	mv	deplacer un fichier
* `!play`	!py	jouer une musique :-) oui jouer une * musique en console
* `!pause` !p	mettre en pause la  musique
* `!resume`	!r	resume musique
* `!stop`	!s	stopper la musique
* `!find`	!f	trouver un fichier par son nom
* `start`	!run	demarrer une application
* `!home`	!h	revenir au dossier su programme	
* `!view`	!vw	afficher une image
* `exit` Exit
* [`Enter`] Afficher le dossier courant
